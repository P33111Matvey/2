global find_word
extern string_equals

section .text

; rdi - адрес ключа
; rsi - адрес последнего элемента в словаре
; (ret) rax - адрес на значение по ключу

find_word:
	push r13
	push r14
	xor rax, rax
.loop:
	test rsi, rsi
	jz .end
	mov r13, rsi
	add rsi, 8
	mov r14, rdi
	call string_equals
	mov rdi, r14
	mov rsi, r13
	test rax, rax
	jnz .found
	mov rsi, [rsi] ; след элем
	jmp .loop
.found:
	mov rax, rsi
.end:
	pop r14
	pop r13
	ret
