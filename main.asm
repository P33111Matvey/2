extern print_newline
extern string_length
extern find_word
extern read_word
extern print_string
extern exit
extern print_char
global _start

section .data

enter_key: db "Please, enter key to find: ", 0
found_value: db "Founded value: ", 0
key_error: db "Key not found.", 0

section .text

%include "colon.inc"
%include "words.inc"

_start:

	;выделим место в стеке для вводимой строки
	push rbp
	mov rbp, rsp
	sub rsp , 256
	
	mov rdi, enter_key
	call print_string
	
	mov rdi, rsp
	mov rsi, 256
	;читаем слово
    call read_word
	
	mov rdi, rax
    mov rsi, prev_pair
    ;поиск в словаре
	call find_word
    test rax, rax
    jz .key_not_found
	;вывод значения
	push rdi
	push rax
	mov rdi, found_value
	call print_string
	mov rdi, '"'
	call print_char
    pop rax
	pop rdi
	add rax, 8
	push rax
    mov rdi, rax
	call string_length
	pop rdi 
    add rdi, rax
    inc rdi
    call print_string
	push rdi
	mov rdi, '"'
	call print_char
	pop rdi
    call print_newline
	jmp .finish
	;вывод ошибки
.key_not_found:
	mov rdi, key_error
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
.finish:
	mov rsp, rbp 
	pop rbp 
	xor rdi,rdi
	call exit
